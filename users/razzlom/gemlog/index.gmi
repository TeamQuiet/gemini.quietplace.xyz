# RaZZlom's Gemlog

Posts written by me. Mix of Russian and English texts. From newest to oldest.

=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/atom.xml Gemini atom feed.
=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/atom_http.xml HTTP atom feed.

## 2022

=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/2022-04-03-MY008.gmi 2022-04-03 Микроблогинг в Gemini: tinylog и gtl / Microbloging in Gemini: tinylog and gtl
=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/2022-04-03-MY007.gmi 2022-04-03 Просто хочу сказать спасибо. / Just want to say thank you.
=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/2022-03-20-MY006.gmi 2022-03-20 RE: What do you self host? [Updated 2022-09-01]
=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/2022-02-12-MY005.gmi 2022-02-12 Scuttlebutt
=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/2022-01-29-MY004.gmi 2022-01-29 Устанавливаем новый go на старый debian

## 2021

=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/2021-02-13-MY003.gmi 2021-02-13 Visual Novels

## 2020

=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/2020-08-29-MY002.gmi 2020-08-29 "Ночь, улица, фонарь, аптека" и "Ворон"
=> gemini://gemini.quietplace.xyz/~razzlom/gemlog/2020-06-06-MY001.gmi 2020-06-06 Создаём блог с Zola

