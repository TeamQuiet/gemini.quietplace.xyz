# С Днём Рождения quietplace.xyz! / Happy Birthday quietplace.xyz!

## RU

С опозданием всего на неделю (которую я лежал с температурой 39С) я хочу сообщить вам радостную новость, 16 октября 2021 года в 21:00 по МСК quietplace.xyz исполнилось ровно один год! Ура товарищи! 

За этот год произошло много всего интересного и не очень. Мы сменили миллион версий Мисскея, сменили машину на более мощную и открыли регистрацию!

Конечно же ничего этого не было бы без моих любимых пользователей. Спасибо Вам огромное!

Так как я не смог придумать ничего праздничного, то я поделюсь с вами небольшой статистикой:

За этот год на QP зарегистрировалось 93 пользователя которые написали 13705 постов.

За последние 7 дней активны были:

* @aurel1on_sol@quietplace.xyz
* @razzlom@quietplace.xyz
* @airin@quietplace.xyz
* @pytat0@quietplace.xyz
* @gakamine@quietplace.xyz
* @blue@quietplace.xyz
* @chipa@quietplace.xyz
* @vovanium@quietplace.xyz
* @Voice@quietplace.xyz
* @liebook@quietplace.xyz
* @kvietcong@quietplace.xyz
* @Thunderlel@quietplace.xyz
* @shamorg@quietplace.xyz

Так же нам известно об 57661 пользователе с 3977 инстансов, которые написали 3446267 постов.

Хочу поблагодарить @drq@mastodon.ml и @mizuki@kawaii.kitsune.cafe за то что помогли мне установить и настроить QP!

Так же хочу выразить мою большую и тёплую благодарность @blue@quietplace.xyz, @vovanium@quietplace.xyz и @liebook@quietplace.xyz за их ежемесячные пожертвования!

С Днём Рождения QP! Поздравляю всех причастных и не очень с этим событием, и помните, наш автобус следует в ад!

## EN

Just a week late (which I was lying with a fever of 39C), I want to glad to tell you joyful news, on October 16, 2021 at 21:00 MSK. quietplace.xyz will be exactly one year old! Hurray comrades! 

During this year, a lot of interesting and not so interesting happened. We've changed a million versions of Misskey, changed the machine to a more powerful one and opened registration!

Of course none of this would be possible without my favorite users. Thank you so much!

Since I couldn't think of anything festive to celebrate, I'll share with you a little statistics:

This year 93 users have registered on QP and written 13705 posts.

Active in the last 7 days were:

* @aurel1on_sol@quietplace.xyz
* @razzlom@quietplace.xyz
* @airin@quietplace.xyz
* @pytat0@quietplace.xyz
* @gakamine@quietplace.xyz
* @blue@quietplace.xyz
* @chipa@quietplace.xyz
* @vovanium@quietplace.xyz
* @Voice@quietplace.xyz
* @liebook@quietplace.xyz
* @kvietcong@quietplace.xyz
* @Thunderlel@quietplace.xyz
* @shamorg@quietplace.xyz

We also know of 57661 users from 3977 instances who have written 3446267 posts.

I'd like to thank @drq@mastodon.ml and @mizuki@kawaii.kitsune.cafe for Helping me to install and configure QP!

I would also like to extend my big and warm thanks to @blue@quietplace.xyz, @vovanium@quietplace.xyz and @liebook@quietplace.xyz for their monthly donations!

Happy Birthday QP! Congratulations to everyone involved and not so much on the occasion, and remember, our bus is going to hell!

