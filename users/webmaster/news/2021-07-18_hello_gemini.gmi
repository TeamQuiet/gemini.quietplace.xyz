# Привет Gemini! / Hello Gemini!

## RU

Совсем недавно узнал о Gemini и сразу же решил сделать свою капсулу. Сейчас тут совсем мало информации, она была скопирована с моей веб странички. Постараюсь в будущем написать ещё.

## EN

Just recently I found out about Gemini and immediately decided to make my capsule. There is very little information here now, it was copied from my web page. I will try to write more in the future.

